package com.sixsev.sqlitetest.mode

/**
 * DESCRIPTION
 * com.sixsev.sqlitetest.mode
 * Created by six.sev on 2017/10/10.
 */
data class Student(val studentId: String, val name: String, val age: Int, val grade: Int)