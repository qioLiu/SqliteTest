package com.sixsev.sqlitetest.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.sixsev.sqlitetest.R
import com.sixsev.sqlitetest.mode.Student
import com.sixsev.sqlitetest.util.DBUtils
import com.sixsev.sqlitetest.util.ThreadPoolUtil
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var dbUtil: DBUtils? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dbUtil = DBUtils(this)
        initView()
    }

    fun initView(){
        tv_insert.setOnClickListener { insertStudent() }
        tv_delete.setOnClickListener { deleteStudent() }
        tv_update.setOnClickListener { updateStudent() }
        tv_query.setOnClickListener { queryStudent() }
    }

    private fun insertStudent() {
        val stu = Student("10010", "lisi", 20, 1)
        ThreadPoolUtil.get().execute(Runnable { dbUtil!!.insert(stu) })
    }

    private fun deleteStudent() {
        ThreadPoolUtil.get().execute(Runnable { dbUtil!!.delete("10010") })
    }

    private fun updateStudent() {
        val stu = Student("10010", "zhangsan", 18, 1)
        ThreadPoolUtil.get().execute(Runnable { dbUtil!!.update(stu) })
    }

    private fun queryStudent() {
        ThreadPoolUtil.get().execute(Runnable { dbUtil!!.query("", arrayOf<String>()) })
    }
}
