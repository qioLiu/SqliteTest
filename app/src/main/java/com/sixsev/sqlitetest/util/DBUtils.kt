package com.sixsev.sqlitetest.util

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.sixsev.sqlitetest.mode.Student
import com.sixsev.sqlitetest.sqlite.SqliteDbHelper

/**
 * DESCRIPTION
 * com.sixsev.sqlitetest.util
 * Created by six.sev on 2017/10/10.
 */
class DBUtils {
    var sqliteHelper: SqliteDbHelper? = null
    constructor(context: Context?){
        sqliteHelper = SqliteDbHelper(context)
    }

    /**
     * 插入一条记录
     * sql语句：insert into table_name (property1, property2....) values (value1, value2,....)
     */
    fun insert(student: Student){
        var wdb: SQLiteDatabase? = null
        try {
            wdb = sqliteHelper!!.writableDatabase
            wdb.beginTransaction()

            var values = ContentValues()
            values.put("studentId", student.studentId)
            values.put("name", student.name)
            values.put("age", student.age)
            values.put("grade", student.grade)
            wdb.insertOrThrow(SqliteDbHelper.TABLE_NAME, null, values)

            wdb.setTransactionSuccessful()
        }catch (e: Exception){

        }finally {
            wdb!!.endTransaction()
            wdb!!.close()
        }

    }

    /**
     * 删除记录
     * sql语句：delete from table_name where ...
     */
    fun delete(studentId: String){
        var wdb: SQLiteDatabase? = null
        try {
            wdb = sqliteHelper!!.writableDatabase
            wdb.beginTransaction()

            val whereString = "studentId = ?"
            wdb.delete(SqliteDbHelper.TABLE_NAME, whereString, arrayOf(studentId))

            wdb.setTransactionSuccessful()
        }catch (e: Exception){

        }finally {
            wdb!!.endTransaction()
            wdb!!.close()
        }

    }

    /**
     * 根据id更新记录
     * sql语句： update table_name set property1=value1, property1=values... where ...
     */
    fun update(student: Student){
        var wdb: SQLiteDatabase? = null
        try {
            wdb = sqliteHelper!!.writableDatabase
            wdb.beginTransaction()

            val whereString = "studentId = ?"
            var values = ContentValues()
            values.put("name", student.name)
            values.put("age", student.age)
            values.put("grade", student.grade)
            wdb.update(SqliteDbHelper.TABLE_NAME, values, whereString, arrayOf(student.studentId))

            wdb.setTransactionSuccessful()
        }catch (e: Exception){

        }finally {
            wdb!!.endTransaction()
            wdb!!.close()
        }

    }


    /**
     * 查询语句
     * sql: select property1, property2.. from table_name where ... groupby .. having.. orderby ..
     */
    fun query(whereString: String, args: Array<String>): MutableList<Student>{
        var rdb: SQLiteDatabase? = null
        var cursor: Cursor? = null
        var students = mutableListOf<Student>()
        try {
            rdb!!.beginTransaction()

            cursor = rdb.query(SqliteDbHelper.TABLE_NAME, SqliteDbHelper.STUDENT_COLUMN, whereString, args, null, null, null)
            if(cursor.count > 0){
                cursor.moveToFirst()
                do{
                    with(cursor){
                        val id = getString(getColumnIndex(SqliteDbHelper.STUDENT_ID))
                        val name = getString(getColumnIndex(SqliteDbHelper.STUDENT_NAME))
                        val age = getInt(getColumnIndex(SqliteDbHelper.STUDENT_AGE))
                        val grade = getInt(getColumnIndex(SqliteDbHelper.STUDENT_GRADE))
                        val student = Student(id, name, age, grade)
                        students.add(student)
                    }
                } while (cursor.moveToNext())
            }

            rdb!!.setTransactionSuccessful()
        }catch (e: Exception){

        }finally {
            rdb!!.endTransaction()
            cursor!!.close()
            rdb!!.close()
        }

        return students
    }
}