package com.sixsev.sqlitetest.util

import java.util.concurrent.*

/**
 * DESCRIPTION
 * com.sixsev.sqlitetest.util
 * Created by six.sev on 2017/10/10.
 */
class ThreadPoolUtil {

    val CORE_SIZE = 5
    val MAX_SIZE = 7
    val KEEP_ALIVE_TIME = 2000L
    val queue = ArrayBlockingQueue<Runnable>(5, true)

    var executor: ThreadPoolExecutor? = null

    companion object{
        fun get(): ThreadPoolUtil{
            return Inner.threadPool
        }
    }

    private object Inner{
        val threadPool = ThreadPoolUtil()
    }

    init {
        executor = ThreadPoolExecutor(CORE_SIZE, MAX_SIZE, KEEP_ALIVE_TIME, TimeUnit.MILLISECONDS, queue)
    }

    fun execute(task: Runnable): Future<*>? {
        if(executor == null){
            executor = ThreadPoolExecutor(CORE_SIZE, MAX_SIZE, KEEP_ALIVE_TIME, TimeUnit.MILLISECONDS, queue)
        }
        val submit = executor!!.submit(task)
        return submit
    }

    fun cancel(task: Runnable){
        executor!!.remove(task)
    }
}