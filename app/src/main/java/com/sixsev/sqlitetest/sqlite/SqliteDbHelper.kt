package com.sixsev.sqlitetest.sqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

/**
 * DESCRIPTION
 * com.sixsev.sqlitetest.sqlite
 * Created by six.sev on 2017/10/10.
 */
class SqliteDbHelper : SQLiteOpenHelper {
    companion object{
        val DATABASE_NAME = "student.db"
        val DATABASE_VERSION = 1
        val TABLE_NAME = "student"

        val STUDENT_ID = "studentId"
        val STUDENT_NAME = "name"
        val STUDENT_AGE = "age"
        val STUDENT_GRADE = "grade"

        val STUDENT_COLUMN = arrayOf(STUDENT_ID, STUDENT_NAME, STUDENT_AGE, STUDENT_GRADE)
    }

    constructor(context: Context?) : super(context, DATABASE_NAME, null, DATABASE_VERSION)

    override fun onCreate(db: SQLiteDatabase?) {
        val sql = "create table if not exists $TABLE_NAME ($STUDENT_ID int primary key, $STUDENT_NAME varchar(20), $STUDENT_AGE int, $STUDENT_GRADE int)"
        db!!.execSQL(sql)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        /**
         * 若需要删除表的某一个字段不能通过alert table table_name drop 字段来删除， 因为android的sqlite不支持这个语句
         * alter table table_name rename to new_name 重命名表
         * alter table table_name add column property_name property_type 增加字段
         * 只能建立一个临时表将当前数据全部复制过来， 然后删掉当前表，重建一个新的表， 然后将临时表的数据拷贝过来， 删除临时表
         */
    }
}